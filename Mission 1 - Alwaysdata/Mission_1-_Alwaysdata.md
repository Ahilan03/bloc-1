# Mission 1 - Alwaysdata

**Objectif : Déployer avec succès une application sur Alwaysdata en documentant les étapes du processus.**

**Étape 0. Récupération des fichiers depuis GitLab :**
- Accédez au formulaire de l'application sur GitLab en suivant le chemin : https://gitlab.com/bts1-sio/bloc-1 (Mission 1 - Alwaysdata, site_web).
- Cloner le répertoire afin de récupérer sur votre machine les 4 fichiers composant l'application. 

**Étape 1. Test local avec XAMPP ou MAMP :**
- Vérifiez l'installation de XAMPP ou MAMP sur votre machine.
- Transférez les fichiers de l'application dans le répertoire approprié du serveur local.
- Configurez et effectuez des tests locaux pour garantir le bon fonctionnement de l'application.

**Étape 2. Validation par le professeur :**
- Présentez les résultats des tests locaux au professeur avant de passer à l'étape suivante.
- Discutez des fonctionnalités réussies et des éventuels problèmes rencontrés.
- Obtenez l'approbation du professeur pour passer à l'étape suivante.

*Basez-vous entièrement sur la documentation officielle d'Alwaysdata à partir de cette étape, en particulier pour la configuration du FTP, du HTTPS, et MySQL.*

**Étape 3. Préparation pour le déploiement sur Alwaysdata :**
- Créez un compte gratuit (Pack 100 Mo) sur Alwaysdata si vous n'en avez pas déjà un.
- Configurez une nouvelle base de données via l'interface Alwaysdata.
- Assurez-vous que l'ensemble du site sera accessible via le protocole HTTPS.

**Étape 4. Transfert des fichiers sur Alwaysdata :**
- Utilisez le protocole FTP pour transférer les fichiers de l'application vers Alwaysdata, à l'aide de l'outil FileZilla Client.

**Étape 5. Tests sur Alwaysdata :**
- Testez l'application sur Alwaysdata pour confirmer son bon fonctionnement.
- Documentez les problèmes rencontrés et les solutions apportées.

**Étape 6. Documentation du Processus :**
- Rédigez une documentation détaillée, étape par étape, du processus de déploiement sur Alwaysdata.
- Incluez des captures d'écran illustrant les différentes étapes.
- Expliquez toute configuration spécifique à Alwaysdata.
- Insérez l'URL dans votre documentation permettant de visualiser l'application en ligne.

**Étape 7. Remise :**
- Soumettez la documentation, et toute information pertinente.
- Respectez la date limite de remise fixée au 2 février 2024 à 23h59.
- Déposez l'ensemble des travaux sur GitLab sous le nom : mission1_alwaysdata, en format PDF ou MD.